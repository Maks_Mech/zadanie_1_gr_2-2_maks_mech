%7.1
h = 6.62607015e-34 % Planck constant 
h / (2*pi) % h bar

%7.2
R = sind(30/exp(1)) % sind - sin of degrees

%7.3
D = hex2dec('00123d3')/2.455e23 % hex2dec - changing hexagonal to decimal

%7.4
sqrt(exp(1)-pi) % square root

%7.5
floor(pi*10^10)-floor(pi*10^9)*10

%7.6
datenum(date) - datenum('19-May-2001', 'dd-mmm-yyyy')

%7.7
Rz = 6371
atan((exp(1)^(sqrt(7)/2)-log(Rz/10^5))/hex2dec('aabb'))

%7.8
mol = 6.02214076e23
Ni = 1/5 * mol *10^-6; % number of particles
N_c2h5oh = 2+5+1+1; % number of atoms in one particle
N = Ni / N_c2h5oh % number of atoms

%7.9
%C2H5OH
masa = 2*12 + 6 * 1 + 16; % mass of whole particle
pro_C = 2*12 / masa; % mass of carbon
ilosc = round(N * pro_C / 101); % number of carbon 12
pro = ilosc / N %  of carbon 12
